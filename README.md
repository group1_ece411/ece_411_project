# ECE_411 Project: IOT Temperature Controller

![pcb-front](Photos/PCB_Front.png)

## Purpose: 
To create an automatic temperature control system based on the ESP8266 microcontroller.


## Overview:
Temperature controller which activates a relay based on current temperature and user specifications.

### Will include:

1. Temperature sensor

2. LCD display

3. Webpage for convenient control

4. Watchdog system for added safety

#### Must:

1. Must include a display which indicates current temperature to user

2. Must include a watchdog system to reset system in the case of crashes

3. Must be able to measure temperature at ranges typical of Western Oregon

4. Must be able to control a relay to toggle heating on/off

5. Must not control relay until initial setup has been completed

6. Must have a manual override

#### Should:

1. Should be able to remotely control system for convenient use.

#### May:

1. May support both Fahrenheit and Celsius

2. May be accessed remotely for control away from home

3. May be able to control either heating or cooling

4. May include face buttons for use without remote 


## To run this code:

Download the following:

https://github.com/BetaRavener/uPyLoader

https://www.python.org/ftp/python/3.5.4/python-3.5.4-amd64.exe

http://micropython.org/resources/firmware/esp8266-20180511-v1.9.4.bin

1. install python 3.5 on PC

2. Get esptool using pip

3. cd to the python35/scripts directory

4. esptool.exe --port COM7 erase_flash

5. esptool.exe --port COM7 --baud 460800 write_flash --flash_size=detect -fm dio 0 PATH_TO\esp8266-20180511-v1.9.4.bin

6. Use uPyLoader to move over boot.py, main.py, wifimgr.py

