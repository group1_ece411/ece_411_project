# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
#If no network config file, then create one.
# Then, create a webpage that lets you type in the network usename and pass.
# User/pass will be stored as a key/val pair and stored in json file

import gc
import os
#import webrepl
#webrepl.start()
#Okay prompt for webpage
def ok(socket,netconfig):
    socket.write("HTTP/1.1 OK\r\n\r\n")
    socket.write("<!DOCTYPE html><title>Web Server</title><body>")
    socket.write("<form method='post' action='/save?"+query.decode()+"'>"+
                 "<input type='text' name='Username' /> <br/>"+
                 "<input type='text' name='Password' /> <br/>"+
                 "<input type='submit' value='Save' />"+
                 "</form>")

#Webpage error printing
def err(socket,netconfig):
    socket.write("HTTP/1.1 "+code+" "+message+"\r\n\r\n")
    socket.write("<h1>"+message+"</h1>")

#Webpage input handler
def handle(socket,netconfig):
    import re
    (method, url, version) = socket.readline().split(b" ")
    if b"?" in url:
        (path, query) = url.split(b"?", 2)
    else:
        (path, query) = (url, b"")
    while True:
        header = socket.readline()
        if header == b"":
            return
        if header == b"\r\n":
            break

    if version != b"HTTP/1.0\r\n" and version != b"HTTP/1.1\r\n":
        err(socket, "505", "Version Not Supported")
    elif method == b"GET":
        if path == b"/":
            ok(socket,netconfig)
        else:
            err(socket, "404", "Not Found")
    elif method == b"POST":
        if path == b"/save":
            
            ok(socket,netconfig)
        else:
            err(socket, "404", "Not Found")
    else:
        err(socket, "501", "Not Implemented")
    return(manualoverride, manualon)

#Variables used for server
server = socket.socket()
server.settimeout(2.0)
addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
server.bind(addr)
server.listen(1)

#Load network credentials from pickle file
netconfig = {}
import json
try:
    with open('netcfg', 'r') as netconfigfile:
        netconfig = json.loads(netconfigfile)[0]
except IOError as e:
    f = open('netcfg', 'wb')
    f.close()
import network
import socket

sta_if = network.WLAN(network.STA_IF)
if not sta_if.isconnected():
    try:
        (socket, addr) = server.accept()
        netconfig = handle(socket, netconfig)
        socket.close()
    except OSError:
        print('.')
    print('connecting to network...')
    sta_if.active(True)
    for username in netconfig:
        sta_if.connect(username, netconfig[username])
    #while not sta_if.isconnected():
    #   pass
print('network config:', sta_if.ifconfig())

gc.collect()
