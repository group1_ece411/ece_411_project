/*
 * Attempts to read from RX1 every 2 seconds
 * If Rx1 is unavailable, assumes that ESP8266 is locked up and sends a reset signal
 * else the system prints out the temperature data received to terminal
 * 
 * The code for including a start and end character for transmission was derived from code provided by Robin2 on the arduino forums:
 * http://forum.arduino.cc/index.php?topic=396450.0
 */


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET 5 // was 4
Adafruit_SSD1306 display(OLED_RESET);

 
//int tempIn;
int serialCounter = 0;
int resetPin = 4;
char temp1[3];

const byte numChars = 27;
char receivedChars[numChars];
boolean newData = false;

void setup() {
  
 Serial.begin(115200); //serial = terminal
 Serial1.begin(115200); //serial1 = RX1
 pinMode(resetPin, OUTPUT);
 digitalWrite(resetPin, LOW);

 display.clearDisplay();
 delay(1000);
 display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
 display.display();
 display.clearDisplay();
 //display temp
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  //pinMode(Serial1, INPUT_PULLUP);
}

void loop() {
    //Variables used for determining start and end of a transmission
    static boolean recvInProgress;  //bool to tell if we are waiting for stop character
    static byte ndx = 0;            //current array location for 
    char startChar = '<';
    char endChar = '>';
    char character;
    //char connecting[12] = 'connecting';
    bool connecting = true;

    //if the ESP is unavailable for 5 consecutive read attempts after initial setup, reset the ESP
    if (serialCounter > 6 && !connecting){
      digitalWrite(resetPin, HIGH);
      delay(1000);
      digitalWrite(resetPin, LOW);
      serialCounter = 0;
    }
       
    if (Serial1.available() > 0)
    {
      //reception
      while (Serial1.available() > 0 && newData == false) {
        character = Serial1.read();
        
        if (recvInProgress == true) {
            if (character != endChar) {
                //we haven't encountered the end of transmission character so append the data to buffer
                receivedChars[ndx] = character;
                ndx++;
                if (ndx >= numChars) {
                    //if transmission was longer than our buffer then chop off last characters
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; //add terminate character to array for printing
                recvInProgress = false;    //Recieve no longer in progress
                ndx = 0;                   //next read starts writing to array from index 0
                newData = true;            //the data we have read is new and needs to be printed
                Serial.print("end");
            }
        }

        else if (character == startChar) {
            //upon seeing start character set transmission boolean to true
            recvInProgress = true;
            Serial.print("recv");
        }
    }
    //verify if data received was valid
    if(isPrintable(receivedChars[0]))
    {
        serialCounter = 0;
        newData = false;

        if(strcmp(receivedChars,"CON")==0){
            connecting = true;
            display.setTextSize(2);
            display.setTextColor(WHITE);
            display.setTextWrap(false);
            display.setCursor(10,0);
            display.clearDisplay();

            
            
            display.println("Connecting...");
            display.display();
            delay(1);
 
            display.startscrollleft(0x00, 0x0F);
            //display.println("PW: Heater");
            
            //display.println("Setup IP:");
            //display.println("192.168.4.1");
            delay(2000);
           
          Serial.print("connect to ap");
        }
        else if((receivedChars[0]-'0') < 10){
          connecting = false;
          Serial.print("The temp is: ");
          Serial.write(receivedChars);
          Serial.print('\n');
          //Display temp on OLED
          display.setTextSize(2);
          display.setTextColor(WHITE);
          display.setCursor(0,0);
   
          //display.print("The temp is ");
          display.print(receivedChars);
          //display.print(temp1[1]);
       
          display.print((char)247); //display degree symbol
          display.print("C");
        }
        //serialCounter = 0;

       //read temp from sensor
       //temp1[0] = Serial1.read();
       //temp1[1] = Serial1.read();
       
       //Serial.print("The temp is: ");
       //Serial.write(temp1[0]);
       //Serial.write(temp1[1]);
       //Serial.println(" ");
       display.display();
       delay(1000);
       display.clearDisplay();
       //delay(1000);
    }
    else if(!connecting){
        serialCounter ++;
        Serial.print("Data is invalid, attempt number: ");
        Serial.println(serialCounter);
    }
        
  }

  else if(!connecting){ 
      serialCounter++;
      Serial.print("serial is unavailable, attempt number: ");
      Serial.println(serialCounter);
  }
  //delay(2000);

}
