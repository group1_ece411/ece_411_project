/*
 * Attempts to read from RX1 every 2 seconds
 * If Rx1 is unavailable, assumes that ESP8266 is locked up and sends a reset signal
 * else the system prints out the temperature data received to terminal
 * 
 * The code for including a start and end character for transmission was derived from code provided by Robin2 on the arduino forums:
 * http://forum.arduino.cc/index.php?topic=396450.0
 */
//int tempIn;
int serialCounter = 0;
int resetPin = 4;


const byte numChars = 8;
char receivedChars[numChars];
boolean newData = false;

void setup() {
  
 Serial.begin(9600); //serial = terminal
 Serial1.begin(9600); //serial1 = RX1
 pinMode(resetPin, OUTPUT);
 digitalWrite(resetPin, LOW);
}

void loop() {
    //Variables used for determining start and end of a transmission
    static boolean recvInProgress;  //bool to tell if we are waiting for stop character
    static byte ndx = 0;            //current array location for 
    char startChar = '<';
    char endChar = '>';
    char character;

    //if the ESP is unavailable for 5 consecutive read attempts, reset the ESP
    if (serialCounter > 4){
      digitalWrite(resetPin, HIGH);
      delay(1000);
      digitalWrite(resetPin, LOW);
      serialCounter = 0;
    }
       
    if (Serial1.available() > 0)
    {
      
      while (Serial1.available() > 0 && newData == false) {
        character = Serial1.read();
        
        if (recvInProgress == true) {
            if (character != endChar) {
                //we haven't encountered the end of transmission character so append the data to buffer
                receivedChars[ndx] = character;
                ndx++;
                if (ndx >= numChars) {
                    //if transmission was longer than our buffer then chop off last characters
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; //add terminate character to array for printing
                recvInProgress = false;    //Recieve no longer in progress
                ndx = 0;                   //next read starts writing to array from index 0
                newData = true;            //the data we have read is new and needs to be printed
            }
        }

        else if (character == startChar) {
            //upon seeing start character set transmission boolean to true
            recvInProgress = true;
        }
      }
      //print what we have read and set newData to false
      if(isPrintable(receivedChars[0]))
      {
        serialCounter = 0;
        Serial.print("The temp is: ");
        Serial.write(receivedChars);
        Serial.print('\n');
        newData = false;
      }
      else{
        serialCounter ++;
        Serial.print("Data is invalid, attempt number: ");
        Serial.println(serialCounter);
      }
        
    }

    else{ 
      serialCounter++;
      Serial.print("serial is unavailable, attempt number: ");
      Serial.println(serialCounter);
    }
    delay(2000);

}
