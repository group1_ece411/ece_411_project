#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <SoftwareSerial.h>
#define DHTPIN            4         // Pin which is connected to the DHT sensor.

// Uncomment the type of sensor in use:
//#define DHTTYPE           DHT11     // DHT 11 
#define DHTTYPE           DHT22     // DHT 22 (AM2302)
//#define DHTTYPE           DHT21     // DHT 21 (AM2301)

DHT_Unified dht(DHTPIN, DHTTYPE);

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

uint32_t delayMS;
boolean firstContact = false;
int ReadPin = 0;   // Pin 0 is UART RX pin
int inByte = 0;    // incoming serial byte
SoftwareSerial portone(8, 9);

//SoftwareSerial portone(8, 9);
void setup() {
  // start serial
  Serial.begin(9600);
  portone.begin(9600);
}

void loop() {
  
        // send data only when you receive data:
        portone.listen();
        if (portone.available() > 0) {
          // read the incoming byte:    
          inByte = Serial.read();
          // say what you got:
          if(inByte != -1)
          {
              Serial.print("I received: ");
              Serial.println(inByte, DEC);
          }
        }
 //       else
          //Serial.print("NOTHING!\n");
}
