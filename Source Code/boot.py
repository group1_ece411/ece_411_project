# Network code was retrieved from https://docs.micropython.org/en/latest/esp8266/tutorial/network_basics.html
# wifimanager for Micropython code retrieved from https://github.com/tayfunulu/WiFiManager
# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import gc
from utime import sleep_ms
import wifimgr
#import webrepl
#webrepl.start()

#Connect to the network
def connect():
    import network
    print('>>>>>>')
    sleep_ms(500)
    print('<CON>')
    sleep_ms(500)
    print('<CON>')
    sleep_ms(500)
    print('<CON>')
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect('SSID', 'PASSWORD')
     #   while not sta_if.isconnected():
      #      pass
    #print('network config:', sta_if.ifconfig())
    wlan = wifimgr.get_connection()
    while wifimgr.get_connection() is None:
        print("Could not initialize the network connection.")
    #while True:
    #    pass  # you shall not pass :D

gc.collect()
sleep_ms(500)
connect()
