/*
 * Attempts to read from RX1 every 2 seconds
 * If Rx1 is unavailable, assumes that ESP8266 is locked up and sends a reset signal
 * else the system prints out the temperature data received to terminal
 * 
 * The code for including a start and end character for transmission was derived from code provided by Robin2 on the arduino forums:
 * http://forum.arduino.cc/index.php?topic=396450.0
 */


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET 5 // was 4
Adafruit_SSD1306 display(OLED_RESET);

 
//int tempIn;
int serialCounter = 0;
int resetPin = 4;
char temp1[3];

const byte numChars = 27;
char receivedChars[numChars];
boolean newData = false;
bool connecting = false;

void setup() {
  
 Serial.begin(115200); //serial = terminal
 Serial1.begin(115200); //serial1 = RX1
 pinMode(resetPin, OUTPUT);
 digitalWrite(resetPin, LOW);

 
 delay(1000);
 display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.clearDisplay();
}

void loop() {
    //Variables used for determining start and end of a transmission
    static boolean recvInProgress;  //bool to tell if we are waiting for stop character
    static byte ndx = 0;            //current array location for 
    char startChar = '<';
    char endChar = '>';
    char character;
    //char connecting[12] = 'connecting';
    

    //if the ESP is unavailable for 5 consecutive read attempts after initial setup, reset the ESP
    if (serialCounter > 6 && !connecting){
      digitalWrite(resetPin, HIGH);
      delay(1000);
      digitalWrite(resetPin, LOW);
      serialCounter = 0;
    }
       
    if (Serial1.available() > 0)
    {
      //reception
      while (Serial1.available() > 0 && newData == false) {
        character = Serial1.read();
        
        if (recvInProgress == true) {
            if (character != endChar) {
                //we haven't encountered the end of transmission character so append the data to buffer
                receivedChars[ndx] = character;
                ndx++;
                if (ndx >= numChars) {
                    //if transmission was longer than our buffer then chop off last characters
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; //add terminate character to array for printing
                recvInProgress = false;    //Recieve no longer in progress
                ndx = 0;                   //next read starts writing to array from index 0
                newData = true;            //the data we have read is new and needs to be printed
                Serial.print("end");
            }
        }

        else if (character == startChar) {
            //upon seeing start character set transmission boolean to true
            recvInProgress = true;
            Serial.print("recv");
        }
    }
    //verify if data received was valid
      if(isPrintable(receivedChars[0]))
      {
        serialCounter = 0;
        newData = false;
        Serial.write(receivedChars);
        
        if(strcmp(receivedChars,"CON")==0){
            connecting = true;
            display.clearDisplay();
            display.setTextSize(2);
            display.setTextColor(WHITE);
            display.setCursor(0,0);
            display.println("Waiting\nfor wifi...");
            Serial.print("wait for wifi");
        }
        else if((receivedChars[0]-'0') < 10){
          connecting = false;
          display.clearDisplay();
          display.setTextSize(4);
          display.setTextColor(WHITE);
          display.setCursor(0,0);
          Serial.print("The temp is: ");
          Serial.write(receivedChars);
          Serial.print('\n');
          int i = 0;
          while((receivedChars[i] != '\0') && (receivedChars[i]-'0' < 10)){
            display.print(receivedChars[i]);
            i = i + 1;
           }
          display.print((char)247); //display degree symbol
          display.print("C");
        }
        else if((receivedChars[0] == 'F')&&((receivedChars[1]-'0') < 10)){
          connecting = false;
          display.clearDisplay();
          display.setTextSize(4);
          display.setTextColor(WHITE);
          display.setCursor(0,0);
          Serial.print("The temp is: ");
          Serial.write(receivedChars);
          Serial.print('\n');
          //display.print(receivedChars);
          int i = 1;
          while((receivedChars[i] != '\0') && (receivedChars[i]-'0' < 10)){
            display.print(receivedChars[i]);
            i = i + 1;
           }
           display.print((char)247); //display degree symbol
           display.print("F");
         }
         else{
          serialCounter ++;
          connecting = false;
          display.clearDisplay();
          display.setTextSize(2);
          display.setTextColor(WHITE);
          display.setCursor(0,0);
          display.println("Not\nconnected");
         }
         display.display();
      }
      else if(!connecting){
        serialCounter ++;
        if(serialCounter > 1){
          display.clearDisplay();
          display.setTextSize(2);
          display.setTextColor(WHITE);
          display.setCursor(0,0);
          display.println("Not\nconnected");
          display.display();
        }
        Serial.print("serial is unavailable, attempt number: ");
        Serial.println(serialCounter);
      }
    }
    else if(!connecting){ 
      serialCounter ++;
      if(serialCounter > 1){
        display.clearDisplay();
        display.setTextSize(2);
        display.setTextColor(WHITE);
        display.setCursor(0,0);
        display.println("Not\nconnected");
        display.display();
      }
      Serial.print("serial is unavailable, attempt number: ");
      Serial.println(serialCounter);
    }
    Serial1.end();    // Ends the serial communication once all data is received to flush buffer
    Serial1.begin(115200);
    delay(2500);
    //Serial1.end();    // Ends the serial communication once all data is received to flush
    //Serial1.begin(115200);
}

