# ESP8266 Temperature and Wifi Control
# Written by: Iain McDonald, Lauren Voepel
# Last modified: 11/4/2018
# Whitequark's code was used as a starting point for this code. Credit given below
# https://lab.whitequark.org/notes/2016-10-20/controlling-a-gpio-through-an-esp8266-based-web-server/


import dht
import machine
from utime import sleep_ms
import socket
from machine import UART
import wifimgr
import os

sleep_ms(2000)

#Definitions for variables used
temperature = dht.DHT11(machine.Pin(5)) #Change this to a DHT11 module
HEATER_GPIO = 4
targettemp = 24
currenttemp = 0
written = 0
manualoverride = False
manualon = False
fahrenheit = False
convert = False
serial = True
heater = machine.Pin(HEATER_GPIO, machine.Pin.OUT)

target_save = 'target.txt'
try:
    f = open(target_save)
    targettemp = int(f.read())
    f.close()
except OSError:
    targettemp = 24

#initialize states of GPIO pins
heater.off()

# Main Code goes here, wlan is a working network.WLAN(STA_IF) instance.

#Okay prompt for webpage
def ok(socket, query, currenttemp, manualoverride, manualon,
       targettemp, fahrenheit, convert, serial):
    socket.write('''<!DOCTYPE html>
<html>
    <head><meta name="viewport" content="width=160,height=200px, initial-scale=1.9," \>
    <meta http-equiv="refresh" content="5;URL=/" />
    <link rel="manifest" href='data:application/manifest+json,{ "name": "Heat Control",
    "short_name": "Heat", "description": "Controls a heater", "display": "standalone"}' />
    
    <style>

.button {background-color: #4CAF50;
    border: none;
    color: white;
    height: 50px;
    width: 152px;
    text-align: center;
    
    text-decoration: none;
    font-size: 16px;
    margin: 4px 0px 0px 0px;
    cursor: pointer;
}
#container{
    text-align: center;
    vertical-align: top;
}
form {
    display: inline;
     
}
div.c {
    text-align: center;
}
html,
body {
  width: 100%;
  height: 100%;
  margin: 0;
}
</style>
</head>
<body>
''')
    #socket.write('')
    
    if (heater.value()) or (manualon == True and manualoverride == True):
        socket.write("""<div class="c"><span style='color:red; font-family:arial;
                         font-size:16px;'>Heater is ON!</span></div>""")
    else:
        socket.write("""<div class="c"><span style='color:blue; font-family:arial;
                         font-size:16px;'>Heater is OFF!</span></div>""")
        
    #socket.write("<br>")
    socket.write("""<div class="c"><span style='font-family:arial;font-size:16px;'>
                 Current temp: """ + str(currenttemp))
    if fahrenheit:
        socket.write("&#176;F</span><br></body></html></div>")
    else:
        socket.write("&#176;C</span><br></body></html></div>")
    
    if manualoverride == False:
        socket.write('<div id="container"><form method="POST" action="/autooff?'+query.decode()+'">'+
                     '''<button class="button">Turn ON Manual-Heat</button></form></div>
                     <div id="container">
                     <form method='POST' action='/tempdown?'''+query.decode()+"'>"+
                     '''<button style="background-color: #0000ff; width: 48px;"
                             class="button">-</button></form>
                        <button style="background-color: #808080; width: 48px;"
                             class="button">'''+ str(targettemp)+ """</button>
                     <form method='POST' action='/tempup?"""+query.decode()+"'>"+
                     '''<button style="background-color: #ff0000; width: 48px;"
                             class="button">+</button></form></div>''')
    else:
        socket.write("<div id='container'><form method='POST' action='/autoon?"+query.decode()+"'>"+
                     '''<button style="background-color: #4CAF50"
                         class="button">Turn ON Auto-Heat</button>
                         </form></div>''')
        #socket.write("<br>")
        if manualon == False:
            socket.write('''<div id="container"><form method='POST' action='/manualon?'''+query.decode()+"'>"+
                         '''<button style="background-color: #ff0000; width: 72px;"
                            class="button">Heat ON</button></form>
                            <button style="background-color: #9f9fff; width: 72px;"
                             class="button">Heat OFF</button></div>'''
                             )
        else:
            socket.write('''<div id="container">
                        <button style="background-color: #ff9f9f; width: 72px;"
                         class="button">Heat ON</button>
                         <form method='POST' action='/manualoff?'''+query.decode()+"'>"+
                         '''<button style="background-color: #0000ff; width: 72px;"
                             class="button">Heat OFF</button>
                            </form></div>''')
    if fahrenheit == False:
        socket.write("<div id='container'><form method='POST' action='/convert?"+query.decode()+"'>"+
                         '''<button
                             class="button">Switch to Fahrenheit</button>
                            </form></div>''')
    else:
        socket.write("<div id='container'><form method='POST' action='/convert?"+query.decode()+"'>"+
                         '''<button 
                             class="button">Switch to Celsius</button>
                            </form></div>''')
    if serial == False:
        socket.write("<div id='container'><form method='POST' action='/serial?"+query.decode()+"'>"+
                         '''<button style="background-color: #333333; height: 30px;"
                             class="button">Enable Serial</button>
                            </form></div>''')
    else:
        socket.write("<div id='container'><form method='POST' action='/serial?"+query.decode()+"'>"+
                         '''<button style="background-color: #333333; height: 30px;"
                             class="button">Disable Serial</button>
                            </form></div>''')

#Webpage error printing
def err(socket, code, message):
    socket.write("HTTP/1.1 "+code+" "+message+"\r\n\r\n")
    socket.write("<h1>"+message+"</h1>")

#Webpage input handler
def handle(socket, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial):
    (method, url, version) = socket.readline().split(b" ")
    if b"?" in url:
        (path, query) = url.split(b"?", 2)
    else:
        (path, query) = (url, b"")
    while True:
        header = socket.readline()
        if header == b"":
            return
        if header == b"\r\n":
            break

    if version != b"HTTP/1.0\r\n" and version != b"HTTP/1.1\r\n":
        err(socket, "505", "Version Not Supported")
    elif method == b"GET":
        if path == b"/":
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        else:
            err(socket, "404", "Not Found")
    elif method == b"POST":
        if path == b"/autoon":
            manualoverride = False
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        elif path == b"/autooff":
            manualoverride = True
            manualon = False
            heater.off()
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        elif path == b"/manualon":
            manualon = True
            heater.on()
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        elif path == b"/manualoff":
            manualon = False
            heater.off()
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        elif path == b"/tempup":
            targettemp = targettemp+1
            f = open(target_save,"w")
            if fahrenheit:
                f.write(str(int(round((targettemp - 32) * (5/9)))))
            else:
                f.write(str(targettemp))
            f.close()
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        elif path == b"/tempdown":
            targettemp = targettemp-1
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        elif path == b"/convert":
            convert = True
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        elif path == b"/serial":
            serial = not serial
            ok(socket, query, currenttemp, manualoverride, manualon, targettemp, fahrenheit, convert, serial)
        else:
            err(socket, "404", "Not Found")
    else:
        err(socket, "501", "Not Implemented")
    return(manualoverride, manualon, targettemp, fahrenheit, convert, serial)

#Serial Data Connection to ATMEGA32U4
#def uart1(temperature):
    
    

#Variables used for server
server = socket.socket()
server.settimeout(2.0)
addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
server.bind(addr)
server.listen(1)

#infinite loop for periodic refresh of webpage
while True:
    #sleep_ms(1000)
    retry = 0
    while retry <3:
        try:
            temperature.measure()
            break
        except:
            retry = retry + 1
            #print("measure retry:" + str(retry))
    currenttemp = temperature.temperature()
    if convert:
        if not fahrenheit:
            targettemp = targettemp * (9/5) + 32
            targettemp = int(round(targettemp))
            fahrenheit = True
            convert = False
        else:
            targettemp = (targettemp - 32) * (5/9)
            targettemp = int(round(targettemp))
            fahrenheit = False
            convert = False
    if serial:
        if fahrenheit:
            currenttemp = currenttemp * (9/5) + 32
            currenttemp = int(round(currenttemp))
            print('>>>>>')
            print('<F' + str(currenttemp) + '>')
            sleep_ms(500)
        else:
            print('>>>>>')
            print('<' + str(currenttemp) + '>')
            sleep_ms(500)
    #written = uart.write('<' + str(currenttemp) + '>')
    #print(written)
    if manualoverride == True:
        if manualon == True:
            heater.on()
        else:
            heater.off()
    else:    
        if currenttemp < targettemp:
            heater.on()
        else:
            heater.off()
    try:
        (socket, addr) = server.accept()
        (manualoverride, manualon, targettemp, fahrenheit, convert, serial) = handle(socket, currenttemp, manualoverride, manualon, targettemp,fahrenheit,convert, serial)
        socket.close()
    except OSError:
        if serial:
            print(".")
